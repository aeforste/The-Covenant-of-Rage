# Dungeon Rules

* Dungeons are dangerous and your characters are being cautious with every action. For this reason, the basic unit of time for actions is **10 minutes**. Exploring a new room takes 10 minutes. Picking a lock takes 10 minutes. Disarming a trap, you guessed it, 10 minutes. Bigger actions will take some multiple of 10 minutes.
* Monsters lurk in darkness, and dungeons are pitch black. If you are in pitch darkness, you are at risk of a random encounter every 10 minutes.
* Torches last 1 hour.
* Dungeons are not overly restful places. Short rests (1 hour) have a chance of failing inside a dungeon, even if you aren't discovered. You can improve your chances by taking precautions e.g. barricading yourself in a room. Long rests are impossible unless STRONGLY justified.
